SPDX Expression|Full Name|URL|Notes
APL-1.0|Adaptive Public License|https://spdx.org/licenses/APL-1.0.html|
APSL-1.0|Apple Public Source License 1.0|https://spdx.org/licenses/APSL-1.0.html|This license is non-free. At one point, it could be found at http://www.opensource.apple.com/apsl/1.0.txt, but that link now redirects to APSL 2.0. A copy of the license text has been taken from archive.org's October 01, 2007 revision.
APSL-1.1|Apple Public Source License 1.1|https://spdx.org/licenses/APSL-1.1.html|This license is non-free. It used to be available here: http://www.opensource.apple.com/apsl/1.1.txt, but that now only points to APSL 2.0. A copy was taken from archive.org's snapshot from September 01, 2000.
APSL-1.2|Apple Public Source License 1.2|https://spdx.org/licenses/APSL-1.2.html|This license is non-free. It used to live at http://www.opensource.apple.com/apsl/1.2.txt, but Apple has taken down all copies of the APSL prior to 2.0. This copy was taken from archive.org's copy from April 15, 2008.
Aladdin|Aladdin Free Public License|https://spdx.org/licenses/Aladdin.html|
Artistic-1.0-Perl|Artistic 1.0 (original)|https://spdx.org/licenses/Artistic-1.0-Perl.html|See: http://www.gnu.org/licenses/license-list.html#ArtisticLicense
BUSL-1.1||https://spdx.org/licenses/BUSL-1.1.html|
CC-BY-NC-1.0|Creative Commons Attribution-NonCommercial 1.0 Generic|https://spdx.org/licenses/CC-BY-NC-1.0.html|
CC-BY-NC-2.0|Creative Commons Attribution-NonCommercial 2.0 Generic|https://spdx.org/licenses/CC-BY-NC-2.0.html|
CC-BY-NC-2.5|Creative Commons Attribution-NonCommercial 2.5 Generic|https://spdx.org/licenses/CC-BY-NC-2.5.html|
CC-BY-NC-3.0-DE|Creative Commons Attribution-NonCommercial 3.0 Germany|https://spdx.org/licenses/CC-BY-NC-3.0-DE.html|
CC-BY-NC-3.0|Creative Commons Attribution-NonCommercial 3.0 Unported|https://spdx.org/licenses/CC-BY-NC-3.0.html|
CC-BY-NC-4.0|Creative Commons Attribution-NonCommercial 4.0 International|https://spdx.org/licenses/CC-BY-NC-4.0.html|
CC-BY-NC-ND-1.0|Creative Commons Attribution-NonCommercial-NoDerivatives 1.0 Generic|https://spdx.org/licenses/CC-BY-NC-ND-1.0.html|
CC-BY-NC-ND-2.0|Creative Commons Attribution-NonCommercial-NoDerivatives 2.0 Generic|https://spdx.org/licenses/CC-BY-NC-ND-2.0.html|
CC-BY-NC-ND-2.5|Creative Commons Attribution-NonCommercial-NoDerivatives 2.5 Generic|https://spdx.org/licenses/CC-BY-NC-ND-2.5.html|
CC-BY-NC-ND-3.0-DE|Creative Commons Attribution-NonCommercial-NoDerivatives 3.0 Germany|https://spdx.org/licenses/CC-BY-NC-ND-3.0-DE.html|
CC-BY-NC-ND-3.0-IGO|Creative Commons Attribution-NonCommercial-NoDerivatives 3.0 IGO|https://spdx.org/licenses/CC-BY-NC-ND-3.0-IGO.html|
CC-BY-NC-ND-3.0|Creative Commons Attribution-NonCommercial-NoDerivatives 3.0 Unported|https://spdx.org/licenses/CC-BY-NC-ND-3.0.html|
CC-BY-NC-ND-4.0|Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International|https://spdx.org/licenses/CC-BY-NC-ND-4.0.html|
CC-BY-NC-SA-1.0|Creative Commons Attribution-NonCommercial-ShareAlike 1.0 Generic|https://spdx.org/licenses/CC-BY-NC-SA-1.0.html|
CC-BY-NC-SA-2.0-FR|Creative Commons Attribution-NonCommercial-ShareAlike 2.0 France|https://spdx.org/licenses/CC-BY-NC-SA-2.0-FR.html|
CC-BY-NC-SA-2.0-UK|Creative Commons Attribution-NonCommercial-ShareAlike 2.0 England and Wales|https://spdx.org/licenses/CC-BY-NC-SA-2.0-UK.html|
CC-BY-NC-SA-2.0|Creative Commons Attribution-NonCommercial-ShareAlike 2.0 Generic|https://spdx.org/licenses/CC-BY-NC-SA-2.0.html|
CC-BY-NC-SA-2.5|Creative Commons Attribution-NonCommercial-ShareAlike 2.5 Generic|https://spdx.org/licenses/CC-BY-NC-SA-2.5.html|
CC-BY-NC-SA-3.0-DE|Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Germany|https://spdx.org/licenses/CC-BY-NC-SA-3.0-DE.html|
CC-BY-NC-SA-3.0-IGO|Creative Commons Attribution-NonCommercial-ShareAlike 3.0 IGO|https://spdx.org/licenses/CC-BY-NC-SA-3.0-IGO.html|
CC-BY-NC-SA-3.0|Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported|https://spdx.org/licenses/CC-BY-NC-SA-3.0.html|
CC-BY-NC-SA-4.0|Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International|https://spdx.org/licenses/CC-BY-NC-SA-4.0.html|
CPOL-1.02|CodeProject Open License (CPOL)|https://spdx.org/licenses/CPOL-1.02.html|Unacceptable use restrictions
EFL-1.0|Eiffel Forum License 1.0|https://spdx.org/licenses/EFL-1.0.html|
EUPL-1.0|European Union Public License v1.0|https://spdx.org/licenses/EUPL-1.0.html|Non-free due to Article 13.
FDK-AAC|Fraunhofer FDK AAC License|https://spdx.org/licenses/FDK-AAC.html|
Frameworx-1.0|Frameworx License|https://spdx.org/licenses/Frameworx-1.0.html|"Non-free due to the limitation on charging for ""Value-Added Services"""
JSON|JSON License|https://spdx.org/licenses/JSON.html|"The clause ""The Software shall be used for Good, not Evil."" is impossible to parse or comply with."
LicenseRef-9wm|9wm License (Original)||The argument that there's a strongly-implied right to modify is unlikely to fly in many countries outside the US. NOTE: 9wm has since been relicensed to MIT.
LicenseRef-AMAP|AMAP License||Additional restrictions are non-free and GPL incompatible
LicenseRef-ARM-SDK|License Agreement for Application Response Measurement (ARM) SDK||
LicenseRef-CACert|CACert Root Distribution License||Liability disclaimer is actually use restriction
LicenseRef-DIP-SIPA-font|DIP SIPA Font License||Restrictions on modification
LicenseRef-IBMsample|IBM Sample Code License||Use restrictions
LicenseRef-IEEE-2017|||
LicenseRef-Intel|Intel IPW3945 Daemon License||No source permissions, binary EULA
LicenseRef-Larabie-font|Larabie Fonts License||Cannot modify
LicenseRef-Literat-font|Literat Font License||Cannot modify
LicenseRef-MS-ss-cli|Microsoft's Shared Source CLI/C#/Jscript License||
LicenseRef-Metasploit-1.2|Metasploit Framework License (pre 2006)||
LicenseRef-NPSL-0.94|Nmap Public Source License Version 0.92 (NPSL)||"The ""Proprietary software companies"" text in the preamble is a field of use restriction."
LicenseRef-NasaCDF|NASA CDF License||Commercial use restrictions, poorly worded
LicenseRef-OpenFlow|||Originally used by the OpenFlow project and later adopted by Mininet. Tolerated in OpenvSwitch and related packages existing in Fedora as of 2022-06-27.
LicenseRef-OpenMotif|Open Motif Public End User License||Commercial use restrictions
LicenseRef-OpenMusic-yellow|LinuxTag Yellow OpenMusic License||Commercial use restrictions
LicenseRef-SCSL|Sun Community Source License||Note as of 2022: link now link broken and webarchive not helpful, but kept here anyway
LicenseRef-Siren14|Siren14 License Agreement||
LicenseRef-Spin|Spin Commercial License||
LicenseRef-SunBCL|Sun Binary Code License Agreement||Non-free, unless resolved with Supplemental License Terms. There are many variants of this, but this is more of a generic identifier
LicenseRef-SystemC-3.0|SystemC Open Source License||link broken, last time this page was recorded was before Accellera acquisition.
LicenseRef-TORQUE|TORQUE v2.5+ Software License v1.0||Commercial use restriction. No link or license text found for v1.0 (v1.1 is allowed in Fedora)
LicenseRef-W3C-20150201|W3C Documentation License||Does not permit modification, different from W3C Software License
LicenseRef-agere-sla|Agere LT Modem Driver License||This license is non-free. A copy of this license was obtained from http://www.mepis.org/node/10356 and is preserved here for reference.
LicenseRef-amazon-sl|Amazon Software License||Section 3.3 is a clear field-of-use restriction
LicenseRef-aptana-1.0|Aptana Public License||No permission to redistribute broken link
LicenseRef-cc-nc-sampling-plus-1.0|Creative Commons Sampling Plus 1.0||
LicenseRef-cmigemo|C/Migemo License||
LicenseRef-commons-clause|Commons Clause||Commercial use restrictions
LicenseRef-free-fork|University of Washington Free Fork License||No longer used
LicenseRef-frontier-1.0|Frontier Artistic License||Non-free for the same reasons as regular Artistic 1.0 License broken link
LicenseRef-helix|Helix DNA Technology Binary Research Use License||No source permissions, binary EULA broken link
LicenseRef-hessla|Hacktivismo Enhanced-Source Software License Agreement||broken link
LicenseRef-iTunes|Apple iTunes License||No source related permissions
LicenseRef-lha|lha license||
LicenseRef-mame|MAME License||Commercial use restrictions. (project appears to have switched to GPLv2)
LicenseRef-mcrae-pl-4-r53|McRae General Public License||Commercial use restrictions
LicenseRef-msntp|MSNTP License||Commercial use restrictions
LicenseRef-odl|Open Directory License||link broken
LicenseRef-osgi-spec-2.0|OSGi Specification License||Forbids modification
LicenseRef-paul-hsieh-derivative|Paul Hsieh Derivative License||
LicenseRef-paul-hsieh-exposition|Paul Hsieh Exposition License||
LicenseRef-pine|Pine License||
LicenseRef-qmail|qmail License||deprecated, qmail is now public domain
LicenseRef-quicktime|Apple Quicktime License||No source related permissions
LicenseRef-scilab-en-2005|Scilab License (OLD)||Current versions of Scilab use the CeCILL license
LicenseRef-sgi-glx-1.0|SGI GLX Public License 1.0||All GLX code now uses Free B 2.0
LicenseRef-squeak|Squeak License||link broken
LicenseRef-stanford-mrouted|mrouted license (old)||
LicenseRef-sun-rpc|Sun RPC License||Restrictions on modification, distribution
LicenseRef-sun-ssscfr-1.1|Sun Solaris Source Code (Foundation Release) License||
LicenseRef-tpl-1.0|Terracotta Public License 1.0||Attribution requirements are burdensome, unclear, and potentially impossible to comply with (Section 14)
LicenseRef-ubuntu-font-1.0|Ubuntu Font License||Poorly written, cannot actually use fonts
LicenseRef-unrar|unrar license||Use restrictions, forced removal clause, see license page for details
LicenseRef-unsplash|Unsplash License||Some use restrictions
LicenseRef-uofu-rfpl|University of Utah Public License||link broken
NASA-1.3|NASA Open Source Agreement v1.3|https://spdx.org/licenses/NASA-1.3.html|
OCCT-exception-1.0|Open CASCADE Technology Public License|https://spdx.org/licenses/OCCT-exception-1.0.html|
ODC-By-1.0||https://spdx.org/licenses/ODC-By-1.0.html|
OGTSL|Open Group Test Suite License|https://spdx.org/licenses/OGTSL.html|Same flaws as Artistic 1.0
OPL-1.0|Open Public License|https://spdx.org/licenses/OPL-1.0.html|
OSLC-2.0|OCLC Public Research License 2.0|https://spdx.org/licenses/OSLC-2.0.html|Appears to limit the price at which a copy can be charged, not just simply prohibiting imposition of royalties.
RPL-1.0|Reciprocal Public License|https://spdx.org/licenses/RPL-1.0.html|
RSCPL|Ricoh Source Code Public License|https://spdx.org/licenses/RSCPL.html|
SGI-B-1.0|SGI Free Software License B 1.1 or older|https://spdx.org/licenses/SGI-B-1.0.html|Version 2.0 is MIT, free and GPL compat.
SSPL-1.0|Server Side Public License v1 (SSPL)|https://spdx.org/licenses/SSPL-1.0.html|
Watcom-1.0|Sybase Open Watcom Public License 1.0|https://spdx.org/licenses/Watcom-1.0.html|
YPL-1.0|Yahoo Public License 1.0|https://spdx.org/licenses/YPL-1.0.html|
Zimbra-1.3|Zimbra Public License 1.3|https://spdx.org/licenses/Zimbra-1.3.html|Badgeware clause does not make allowances for modifications in which it is not technically possible to preserve a 'logo', we must assume this provision constitutes an undue burden on the right of modification.
gSOAP-1.3b|gSOAP Public License|https://spdx.org/licenses/gSOAP-1.3b.html|"Please note that while the ""gSOAP Public License"" is non-free, the ""gSOAP Toolkit"" is dual licensed as GPLv2 or gSOAP Public License."
